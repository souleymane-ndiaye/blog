import {Component, Input, OnInit} from '@angular/core';
import {Post} from '../post';

@Component({
  selector: 'app-post-list-component',
  templateUrl: './post-list-component.component.html',
  styleUrls: ['./post-list-component.component.scss']
})
export class PostListComponentComponent implements OnInit {

@Input() post: Post;

  constructor() { }

  ngOnInit() {
  }
onLove() {
    this.post.loveIts = this.post.loveIts + 1;
    console.log(this.post.loveIts);
}
  onHate() {
    this.post.loveIts = this.post.loveIts - 1;
    console.log(this.post.loveIts);
  }
  getColor() {
    if ( this.post.loveIts > 0) {
      return 'green';
    } else if ( this.post.loveIts < 0) {
      return 'red';
    }
  }

}
