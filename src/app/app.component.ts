import { Component } from '@angular/core';
import { Post } from './post';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  updateDate = new Date();
  posts: Post[] = [
    { title: 'Mon premier post',
      content: ' Information : je vous conseille d\'utiliser Bootstrap pour cet exercice.  Si vous créez des list-group-item  ',
      loveIts: 0,
      created_at: this.updateDate
    },
    { title: 'Mon deuxième post',
      content: ' Information : je vous conseille d\'utiliser Bootstrap pour cet exercice.  Si vous créez des list-group-item ',
      loveIts: 0,
      created_at: this.updateDate
    },
    { title: 'Encore un post',
      content: ' Information : je vous conseille d\'utiliser Bootstrap pour cet exercice.  Si vous créez des list-group-item ',
      loveIts: 0,
      created_at: this.updateDate
    }

  ];
}
